import './layout'
import './pages'
import './styleguide'

import Helper from './helper'

window.onload = function () {
  const helper = new Helper('{{cli_domain}}')
  helper.checkUntransformedImage()
}

