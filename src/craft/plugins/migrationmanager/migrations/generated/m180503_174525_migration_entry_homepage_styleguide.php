<?php

namespace Craft;

/**
 * Generated migration
 */
class m180503_174525_migration_entry_homepage_styleguide extends BaseMigration
{
    /**
    Migration manifest:
    
    ENTRY
        - homepage
        - styleguide
        
    */
	/**
	 * Any migration code in here is wrapped inside of a transaction.
	 * Returning false will rollback the migration
	 *
	 * @return bool
	 */
	public function safeUp()
	{
	    $json = '{"content":{"entries":[{"slug":"homepage","section":"homepage","locales":{"en_ca":{"slug":"homepage","section":"homepage","enabled":"1","locale":"en_ca","localeEnabled":"1","postDate":{"date":"2018-04-24 21:33:21.000000","timezone_type":3,"timezone":"UTC"},"expiryDate":null,"title":"Homepage","entryType":"homepage","body":"<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Todo_app_main_domain.com will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>","seo":{}}}},{"slug":"styleguide","section":"styleguide","locales":{"en_ca":{"slug":"styleguide","section":"styleguide","enabled":"1","locale":"en_ca","localeEnabled":"1","postDate":{"date":"2018-04-26 00:01:51.000000","timezone_type":3,"timezone":"UTC"},"expiryDate":null,"title":"Styleguide","entryType":"styleguide"}}}]}}';
        return craft()->migrationManager_migrations->import($json);    }
}
