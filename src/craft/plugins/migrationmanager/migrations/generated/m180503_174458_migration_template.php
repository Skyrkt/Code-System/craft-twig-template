<?php

namespace Craft;

/**
 * Generated migration
 */
class m180503_174458_migration_template extends BaseMigration
{
    /**
    Migration manifest:
    
    LOCALE
        - en_ca
        
    FIELD
        - body
        - bodyScripts
        - footerScripts
        - headerScripts
        - seo
        - tags
        
    SECTION
        - homepage
        - styleguide
        
    GLOBAL
        - generalScripts
        - productionScripts
        
    */
	/**
	 * Any migration code in here is wrapped inside of a transaction.
	 * Returning false will rollback the migration
	 *
	 * @return bool
	 */
	public function safeUp()
	{
	    $json = '{"settings":{"dependencies":{"locales":[{"id":"en_ca"}],"sections":[{"name":"Homepage","handle":"homepage","type":"single","enableVersioning":"1","hasUrls":"1","template":"pages/homepage","maxLevels":null,"locales":{"en_ca":{"locale":"en_ca","urlFormat":"__home__","nestedUrlFormat":null,"enabledByDefault":"1"}},"entrytypes":[{"sectionHandle":"homepage","hasTitleField":"1","titleLabel":"Homepage","name":"Homepage","handle":"homepage","fieldLayout":{"Content":["body"],"SEO":["seo"]},"requiredFields":["body"]}]},{"name":"Styleguide","handle":"styleguide","type":"single","enableVersioning":"1","hasUrls":"1","template":"styleguide/styleguide/styleguide.twig","maxLevels":null,"locales":{"en_ca":{"locale":"en_ca","urlFormat":"styleguide","nestedUrlFormat":null,"enabledByDefault":"1"}},"entrytypes":[{"sectionHandle":"styleguide","hasTitleField":"0","titleLabel":null,"titleFormat":"{section.name|raw}","name":"Styleguide","handle":"styleguide","fieldLayout":[],"requiredFields":[]}]}]},"elements":{"locales":[{"id":"en_ca"}],"fields":[{"group":"Default","name":"Body","handle":"body","instructions":null,"translatable":"1","required":false,"type":"RichText","typesettings":{"configFile":"Standard.json","columnType":"text","availableAssetSources":[]}},{"group":"Injected Scripts","name":"Body Scripts","handle":"bodyScripts","instructions":"Add scripts right after <body>","translatable":"0","required":false,"type":"PlainText","typesettings":{"placeholder":"","maxLength":"","multiline":"1","initialRows":"4"}},{"group":"Injected Scripts","name":"Footer Scripts","handle":"footerScripts","instructions":"Add scripts before </body>","translatable":"0","required":false,"type":"PlainText","typesettings":{"placeholder":"","maxLength":"","multiline":"1","initialRows":"4"}},{"group":"Injected Scripts","name":"Header Scripts","handle":"headerScripts","instructions":"Add scripts before </head>","translatable":"0","required":false,"type":"PlainText","typesettings":{"placeholder":"","maxLength":"","multiline":"1","initialRows":"4"}},{"group":"SEO","name":"SEO","handle":"seo","instructions":"SEO config for single page","translatable":"0","required":false,"type":"Seomatic_Meta","typesettings":{"assetSources":"*","seoMainEntityCategory":"CreativeWork","seoMainEntityOfPage":"WebPage","seoTitleSource":"field","seoTitleSourceField":"title","seoTitle":"","seoTitleSourceChangeable":"1","seoDescriptionSource":"custom","seoDescriptionSourceField":"title","seoDescription":"","seoDescriptionSourceChangeable":"1","seoKeywordsSource":"custom","seoKeywordsSourceField":"title","seoKeywords":"","seoKeywordsSourceChangeable":"1","seoImageIdSource":"custom","seoImageIdSourceChangeable":"1","seoImageTransform":"","twitterCardType":"","twitterCardTypeChangeable":"1","seoTwitterImageIdSource":"custom","seoTwitterImageIdSourceChangeable":"1","seoTwitterImageTransform":"","openGraphType":"","openGraphTypeChangeable":"1","seoFacebookImageIdSource":"custom","seoFacebookImageIdSourceChangeable":"1","seoFacebookImageTransform":"","robots":"","robotsChangeable":"1"}},{"group":"Default","name":"Tags","handle":"tags","instructions":null,"translatable":"0","required":false,"type":"Tags","typesettings":{"source":"default"}}],"sections":[{"name":"Homepage","handle":"homepage","type":"single","enableVersioning":"1","hasUrls":"1","template":"pages/homepage","maxLevels":null,"locales":{"en_ca":{"locale":"en_ca","urlFormat":"__home__","nestedUrlFormat":null,"enabledByDefault":"1"}},"entrytypes":[{"sectionHandle":"homepage","hasTitleField":"1","titleLabel":"Homepage","name":"Homepage","handle":"homepage","fieldLayout":{"Content":["body"],"SEO":["seo"]},"requiredFields":["body"]}]},{"name":"Styleguide","handle":"styleguide","type":"single","enableVersioning":"1","hasUrls":"1","template":"styleguide/styleguide/styleguide.twig","maxLevels":null,"locales":{"en_ca":{"locale":"en_ca","urlFormat":"styleguide","nestedUrlFormat":null,"enabledByDefault":"1"}},"entrytypes":[{"sectionHandle":"styleguide","hasTitleField":"0","titleLabel":null,"titleFormat":"{section.name|raw}","name":"Styleguide","handle":"styleguide","fieldLayout":[],"requiredFields":[]}]}],"globals":[{"name":"General Scripts","handle":"generalScripts","fieldLayout":{"Content":["headerScripts","bodyScripts","footerScripts"]},"requiredFields":[]},{"name":"Production Scripts","handle":"productionScripts","fieldLayout":{"Content":["headerScripts","bodyScripts","footerScripts"]},"requiredFields":[]}]}}}';
        return craft()->migrationManager_migrations->import($json);    }
}
