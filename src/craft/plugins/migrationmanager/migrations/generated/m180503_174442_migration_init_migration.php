<?php

namespace Craft;

/**
 * Generated migration
 */
class m180503_174442_migration_init_migration extends BaseMigration
{
    /**
    Migration manifest:
    
    LOCALE
        - en_ca
        
    */
	/**
	 * Any migration code in here is wrapped inside of a transaction.
	 * Returning false will rollback the migration
	 *
	 * @return bool
	 */
	public function safeUp()
	{
	    $json = '{"settings":{"dependencies":{"locales":[{"id":"en_ca"}]},"elements":{"locales":[{"id":"en_ca"}]}}}';
        return craft()->migrationManager_migrations->import($json);    }
}
