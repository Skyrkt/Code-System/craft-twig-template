FROM skyrkt/craft:2.6.2982

COPY src/craft/config/general.php /usr/share/nginx/craft/config/general.php
# COPY src/craft/config/license.key /usr/share/nginx/craft/config/license.key
COPY src/craft/config/redactor /usr/share/nginx/craft/config/redactor
COPY src/craft/plugins /usr/share/nginx/craft/plugins/
COPY src/app /usr/share/nginx/craft/templates/
COPY src/public /usr/share/nginx/public/

COPY .devops/docker-files/php.ini /etc/php/7.0/fpm/php.ini
COPY .devops/docker-files/15-expires.conf /opt/docker/etc/nginx/vhost.common.d/15-expires.conf
COPY .devops/docker-files/01-301.conf /opt/docker/etc/nginx/vhost.common.d/01-301.conf
COPY .devops/docker-files/vhost.conf /opt/docker/etc/nginx/vhost.conf
